package com.futuralabs.panda.services;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Shamir on 1/19/2016.
 */
public class Jsonserviceclass {
    public static String responsestring;
    JSONObject jObj;
    public JSONObject getinitialvalues(String h,String did,String i) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse response;
        jObj = new JSONObject();

        try {


            HttpPost post = new HttpPost("http://orders.pandafoods.co.in/api/sink/sinkapp/");

            jObj.put("ud_id","d2d5fb155dc25744");
            jObj.put("initial", i);


            StringEntity se = new StringEntity(jObj.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);

            post.setHeader("AUT", h);
            response = client.execute(post);
           // Log.v("here...news detail", response.toString());
            HttpEntity resEntity = response.getEntity();
            responsestring = EntityUtils.toString(resEntity);
            responsestring = responsestring.trim();
            Log.v("news detail", responsestring+" .");

        } catch (ClientProtocolException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }


}
