package com.futuralabs.panda.utilities;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.futuralabs.panda.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Shamir on 1/19/2016.
 */
public class GridviewCustomadapter extends BaseAdapter{

    String [] cat_name,cat_imageId,cat_id;
    Context context;

    private static LayoutInflater inflater=null;
    public GridviewCustomadapter(Activity mainActivity,String []cat_ids, String[] cat_names, String[] cat_imageIds) {
        // TODO Auto-generated constructor stub
        cat_name=cat_names;
        context=mainActivity;
        cat_imageId=cat_imageIds;
        cat_id=cat_ids;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return cat_name.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.home_gridlist, null);
        holder.tv=(TextView) rowView.findViewById(R.id.textView1);
        holder.img=(ImageView) rowView.findViewById(R.id.image);

        holder.tv.setText(cat_name[position]);

        String url=cat_imageId[position];
//            String nameDta=cursor.getString(cursor.getColumnIndexOrThrow(CatagoryDb.CAT_NAME));
        String Img_Url= "http://orders.pandafoods.co.in/"+url;
        Picasso.with(context).load(Img_Url).into(holder.img);

        rowView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+cat_name[position], Toast.LENGTH_LONG).show();
            }
        });

        return rowView;
    }

}
