package com.futuralabs.panda;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ListView;

import com.futuralabs.panda.services.Jsonserviceclass;
import com.futuralabs.panda.utilities.GridviewCustomadapter;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Shamir on 1/19/2016.
 */
public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
ListView gv;
    ProgressDialog mDialog;
String resp;
    public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDialog=new ProgressDialog(Home.this);
gv=(ListView)findViewById(R.id.gridView1);
        new Getcategories().execute();

       // gv.setAdapter(new GridviewCustomadapter(this,prgmNameList,null));
       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.pandafood) {
            // Handle the camera action
        } else if (id == R.id.login) {
            Intent i=new Intent(this,Login.class);
            startActivity(i);

        } else if (id == R.id.signup) {

        } else if (id == R.id.corlog) {
            Intent i=new Intent(this,Login.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class Getcategories extends AsyncTask<String ,String ,String >{
        String []cat_id,cat_name,cat_img_url;
        JSONArray  Catogoryarray;


        @Override
        protected void onPreExecute() {
            mDialog.setMessage("Please wait...");
            mDialog.setCancelable(false);
            mDialog.show();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            try{
                Jsonserviceclass jservice=new Jsonserviceclass();
                JSONObject jsonObject = jservice.getinitialvalues("f535cc112c4fd53386e436afcf6436d9",
                        "d2d5fb155dc25744","1");
                JSONObject productOb = new JSONObject(Jsonserviceclass.responsestring);
                resp=productOb.getString("details");
                JSONObject details=productOb.getJSONObject("details");
                Log.i("Here it is...test..", details.toString());
                Catogoryarray=details.getJSONArray("category");

                //by bsr
                    cat_id=new String[Catogoryarray.length()];
                    cat_name=new String[Catogoryarray.length()];
                    cat_img_url=new String[Catogoryarray.length()];
                for(int i=0;i<Catogoryarray.length();i++){
                    cat_name[i]=Catogoryarray.getJSONObject(i).getString("cat_name");
                    cat_id[i]=Catogoryarray.getJSONObject(i).getString("cat_id");
                    cat_img_url[i]=Catogoryarray.getJSONObject(i).getString("cat_image");
                }


            }catch (Exception e){
                Log.i("errorHere it is.....", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
           mDialog.dismiss();


            for (int i=0;i<Catogoryarray.length();i++){
                 gv.setAdapter(new GridviewCustomadapter(Home.this,cat_id,cat_name,cat_img_url));
            }



        }
    }
}
